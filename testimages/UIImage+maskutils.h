//
//  UIImage+maskutils.h
//  testimages
//

#import <UIKit/UIKit.h>

@interface UIImage (maskutils)

// in response to this stack overflow question:  http://stackoverflow.com/questions/14622202/can-cgcontextcliptomask-mask-all-non-transparent-pixels-with-alpha-1/
// I started with code for converting to grayscale here:  http://stackoverflow.com/questions/1298867/convert-image-to-grayscale
// and hacked it to make create a stencil
//
// All pixels become full black so alpha controls mask properties.
// then if inCollapseAlpha is YES, then any non-zero alpha becomes 1.0 alpha and any 0 alpha stays zero alpha
// if inCollapseAlpha is NO then alpha values are untouched (so get alpha gradient like in source image).
//
// note caller should release the result

- (UIImage*) createStencilWithCollapseAlpha: (BOOL) inCollapseAlpha;

@end
