//
//  TestView.h
//  testimages
//
//

#import <UIKit/UIKit.h>

@interface TestView : UIView

@property (strong) UIImage *maskimage;
@property (strong) UIImage * mainimage;

@end
