//
//  TestView.m
//  testimages
//
//

#import "TestView.h"
#import "UIImage+maskutils.h"

@implementation TestView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.maskimage = [UIImage imageNamed: @"masker.png"];
        self.mainimage = [UIImage imageNamed: @"sika.jpg"];
    }
    return self;
}

- (void) awakeFromNib
{
    self.maskimage = [UIImage imageNamed: @"masker.png"];
    self.mainimage = [UIImage imageNamed: @"sika.jpg"];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSaveGState( ctx );
    
    CGContextTranslateCTM( ctx, 0, self.frame.size.height );
    CGContextScaleCTM( ctx, 1, -1 );

    // test the mask
    // of course you'd cache this if you were really going to use it over and over like this, but this is just a quick test app.
    
    // returns a UIImage so it has the appropriate scale for retina if the source image was retina.
    // then you use the UIImage size (since it's in points) and only use the CGImage as the last param to CGContextClipToMask
    UIImage * bwimage = [self.maskimage createStencilWithCollapseAlpha: YES];
    if ( bwimage )
        CGContextClipToMask( ctx, CGRectMake( 10, 10, bwimage.size.width, bwimage.size.height ), [bwimage CGImage] );
    
    CGContextDrawImage( ctx, CGRectMake( 10, 10, self.mainimage.size.width, self.mainimage.size.height ), self.mainimage.CGImage );
        
    CGContextRestoreGState( ctx );
}


@end
